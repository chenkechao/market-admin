# uniCloud图表示例

本示例基于uniCloud的[clientDB](https://uniapp.dcloud.net.cn/uniCloud/clientdb)技术，无需云函数，在前端对数据库进行group和count，进行按天的计数统计。

提供了2个图表示例：
- [uchart图表](https://ext.dcloud.net.cn/plugin?id=271)，页面路径为pages/chart/uchart。全端可用。体积较小，几十K，功能不如echart多。
- [echart图表](https://ext.dcloud.net.cn/plugin?id=1668)，页面路径为pages/chart/echart。仅web和App可用。体积较大，730K，功能丰富。

开发者可根据自己需求选择合适的图表。

## 安装步骤
- 前提
1. 确保电脑上已经有uniCloud admin项目，如果还没有，在这里下载一个：[https://ext.dcloud.net.cn/plugin?id=3268](https://ext.dcloud.net.cn/plugin?id=3268)
2. 关联uniCloud admin项目到服务空间。如果是第一次用，要在HBuilderX的uniCloud初始化向导里做初始化，把云函数上传到服务空间，通过db_init.json进行数据库初始化
- 本插件安装
1. 将本插件导入到已存在的uniCloud admin项目
2. 在pages.json中注册2个页面
```json
{
	"path": "uni_modules/wa-chart/pages/chart/uchart",
	"style":{
		"navigationBarTitleText":"uchart示例"
	}
},
{
	"path": "uni_modules/wa-chart/pages/chart/echart",
	"style":{
		"navigationBarTitleText":"echart示例"
	}
}
```
3. 运行uniCloud admin项目，如果是第一次运行需要根据登录界面的提示注册一个管理员账户
4. 在运行后的uniCloud admin的左侧找到菜单管理，添加echart和uchart图表示例菜单，路径分别配为`/uni_modules/wa-chart/pages/chart/uchart`、`/uni_modules/wa-chart/pages/chart/echart`，然后刷新页面，左侧的图表菜单就会出现2个示例

## 示例说明
- 统计的数据表为[uni-id-users](https://gitee.com/dcloud/opendb/blob/master/collection/uni-id-users/collection.json)。在uniCloud后台新建数据表时可选择该表的模板。
- 需要确保前端页面有访问uni-id-users表的权限
	* 或者在[uniCloud admin](https://ext.dcloud.net.cn/plugin?id=3268)中使用管理员登录后再查看本页面报表
	* 或者测试期间也可以在uni-id-users表的表结构schema中在根节点配置`"permission": {"read": true}`，表示该表为前端可读。
- 如果没有注册用户的话，图表的数据都是0，需要在uniCloud admin的 用户管理 里新增用户
- 默认统计7天内的每日注册用户量。时间范围可在代码中修改，修改data中的startdate、enddate即可