const {
	Controller
} = require('uni-cloud-router')
const {
	getConfig
} = require("configs");
module.exports = class ShopController extends Controller {
	async init() {
		let {
			domain
		} = getConfig();
		const currentUserInfo = await this.service.user.getCurrentUserInfo(['_id', 'username', 'admin', 'role'])
		return {
			webConfig: {
				domain
			},
			shops: await this.service.shop.getAdminShops(currentUserInfo.userInfo),
		}
	}
}
