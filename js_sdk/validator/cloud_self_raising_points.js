// 校验规则由 schema 生成，请不要直接修改当前文件，如果需要请在uniCloud控制台修改schema
// uniCloud: https://unicloud.dcloud.net.cn/



export default {
  "address": {
    "rules": [
      {
        "required": true
      },
      {
        "format": "string"
      }
    ],
    "label": "地址"
  },
  "latitude": {
    "rules": [
      {
        "required": true
      },
      {
        "format": "double"
      }
    ],
    "label": "维度"
  },
  "longitude": {
    "rules": [
      {
        "required": true
      },
      {
        "format": "double"
      }
    ],
    "label": "经度"
  },
  "location": {
    "rules": [
      {
        "format": "object"
      }
    ],
    "label": "地理位置"
  },
  "mobile": {
    "rules": [
      {
        "required": true
      },
      {
        "format": "string"
      }
    ],
    "label": "电话"
  },
  "name": {
    "rules": [
      {
        "required": true
      },
      {
        "format": "string"
      }
    ],
    "label": "名称"
  },
  "src": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "封面图"
  }
}
