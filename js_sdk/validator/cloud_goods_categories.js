
// 表单校验规则由 schema2code 生成，不建议直接修改校验规则，而建议通过 schema2code 生成, 详情: https://uniapp.dcloud.net.cn/uniCloud/schema



const validator = {
  "id": {
    "rules": [
      {
        "format": "int"
      }
    ],
    "label": "编号"
  },
  "src": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "图片"
  },
  "isRecommend": {
    "rules": [
      {
        "format": "int"
      },
      {
        "range": [
          {
            "text": "否",
            "value": 0
          },
          {
            "text": "是",
            "value": 1
          }
        ]
      }
    ],
    "defaultValue": 0,
    "label": "是否推荐"
  },
  "isShow": {
    "rules": [
      {
        "format": "int"
      },
      {
        "range": [
          {
            "text": "否",
            "value": 0
          },
          {
            "text": "是",
            "value": 1
          }
        ]
      }
    ],
    "defaultValue": 0,
    "label": "是否显示"
  },
  "name": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "名称"
  },
  "parent_id": {
    "rules": [
      {
        "format": "string"
      }
    ],
    "label": "父id"
  },
  "posid": {
    "rules": [
      {
        "format": "int"
      }
    ],
    "label": "排序，asc"
  },
  "shopid": {
    "rules": [
      {
        "format": "int"
      }
    ],
    "label": "店铺编号"
  }
}

const enumConverter = {
  "isRecommend_valuetotext": {
    "0": "否",
    "1": "是"
  },
  "isShow_valuetotext": {
    "0": "否",
    "1": "是"
  }
}

export { validator, enumConverter }
