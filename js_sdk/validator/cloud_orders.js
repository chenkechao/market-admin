// 表单校验规则由 schema2code 生成，不建议直接修改校验规则，而建议通过 schema2code 生成, 详情: https://uniapp.dcloud.net.cn/uniCloud/schema



const validator = {
	"id": {
		"rules": [{
			"format": "int"
		}],
		"label": "编号"
	},
	"address": {
		"rules": [{
			"format": "object"
		}],
		"label": "地址"
	},
	"body": {
		"rules": [{
			"format": "string"
		}],
		"label": "内容"
	},
	"cancelApply": {
		"rules": [{
			"format": "array"
		}],
		"label": "取消订单请求"
	},
	"cancelStyle": {
		"rules": [{
				"format": "string"
			},
			{
				"range": [{
						"text": "自动",
						"value": "auto"
					},
					{
						"text": "用户",
						"value": "user"
					},
					{
						"text": "商家",
						"value": "shop"
					}
				]
			}
		],
		"label": "取消类型"
	},
	"cartCount": {
		"rules": [{
			"format": "int"
		}],
		"label": "购物车数量"
	},
	"deliveryHour": {
		"rules": [{
			"format": "object"
		}],
		"label": "配送时间"
	},
	"deliveryType": {
		"rules": [{
				"format": "string"
			},
			{
				"range": [{
						"text": "送到家",
						"value": "deliveryHome"
					},
					{
						"text": "自提",
						"value": "selfRaising"
					}
				]
			}
		],
		"label": "配送方式"
	},
	"freight": {
		"rules": [{
			"format": "double"
		}],
		"label": "运费"
	},
	"goods": {
		"rules": [{
			"format": "array"
		}],
		"label": "商品"
	},
	"isDelete": {
		"rules": [{
				"format": "int"
			},
			{
				"range": [{
						"text": "否",
						"value": 0
					},
					{
						"text": "是",
						"value": 1
					}
				]
			}
		],
		"defaultValue": 0,
		"label": "是否删除"
	},
	"juli": {
		"rules": [{
			"format": "double"
		}],
		"label": "距离"
	},
	"lastPayTime": {
		"rules": [{
			"format": "timestamp"
		}],
		"label": "最后支付时间"
	},
	"platform": {
		"rules": [{
			"format": "string"
		}],
		"label": "操作平台"
	},
	"remark": {
		"rules": [{
			"format": "string"
		}],
		"label": "备注"
	},
	"shopid": {
		"rules": [{
			"format": "int"
		}],
		"label": "店铺"
	},
	"state": {
		"rules": [{
			"format": "int"
		}],
		"label": "状态"
	},
	"totalDiscount": {
		"rules": [{
			"format": "double"
		}],
		"label": "优惠金额"
	},
	"totalMoney": {
		"rules": [{
			"format": "double"
		}],
		"label": "总金额"
	},
	"uid": {
		"rules": [{
			"format": "string"
		}],
		"label": "用户"
	}
}

const enumConverter = {
	"cancelStyle_valuetotext": {
		"auto": "自动",
		"user": "用户",
		"shop": "商家",
	},
	"deliveryType_valuetotext": {
		"deliveryHome": "送到家",
		"selfRaising": "自提"
	},
	"isDelete_valuetotext": {
		"0": "否",
		"1": "是"
	}
}

export {
	validator,
	enumConverter
}
